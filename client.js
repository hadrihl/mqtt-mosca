// Client will subscribe to SN
var mqtt = require('mqtt');
var client = mqtt.connect('mqtt://localhost');

client.on('connect', function() {
    client.subscribe('chat');
    
    client.on('message', function(topic, message) {
        
        switch(message.toString()) {
            case "bye":
                client.end();
                console.log("conversation terminated.");
                break;
        }
    });
});