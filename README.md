mqtt-mosca
==========
An experimental of MQTT and Mosca. 

How code works
--------------
1. Client subscribe one particular topic through broker. 

2. Broker will record the subscription.

3. SN will publish the data through broker. 

4. Broker will broadcast to subscribed client. 

How to setup
------------
1. Install dependencies
    * $ npm install --save

2. Run broker
    * $ node broker.js
    
3. Run client (initiate subscribe)
    * $ node client.js

4. Run SN (publish data to broker so that broker will broadcast the data to subscribed client)
    * $ node sn.js

Author
------
* hadrihl // hadrihilmi[at]gmail[dot]com