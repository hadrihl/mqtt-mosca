// SN publish topic 'presence' for client to subscribe
var mqtt = require('mqtt');
var sn = mqtt.connect('mqtt://localhost');

sn.on('connect', function() {
    sn.publish('chat', 'bye', { retain: false, qa: 1 });
    sn.end();
});